﻿using System.Collections.Generic;

namespace LogAPLComparer
{
    class Program
    {
        static void Main(string[] args)
        {
            string folder = @"D:\BattleNet\World of Warcraft\_retail_\Logs\";
            string file = /*"WoWCombatLog-121221_123945.txt";*/ "WoWCombatLog-120321_154735.txt";

            var events = LoggedEvent.LoadEvents(folder + file);
            var uniqueSpellIds = new HashSet<int>();

            foreach(LoggedEvent e in events)
            {
                uniqueSpellIds.Add(e.SpellId);
            }
            var communicator = new WowAPICommunicator();
            foreach(int spellId in uniqueSpellIds)
            {
                string iconUrl = communicator.GetSpellMediaAsync(spellId.ToString()).GetAwaiter().GetResult();
                if(iconUrl is not null)
                {
                    communicator.DownloadIconAsync(iconUrl).Wait();
                }
            }


            //Player p = new Player();
            //int j = 0;
            //for(int i = 0; i < fileLines.Length; i++)
            //{
            //    actions[i] = new LoggedEvent(fileLines[i]);
            //    if(actions[i].CasterUnitFlag == "0x511")
            //    {
            //        if(actions[i].EventType == "SPELL_CAST_SUCCESS")
            //        {
            //            Console.Write(i + ": " + actions[i].SpellName + " - ");
            //            Console.WriteLine(p.GetPlayerAuras());
            //            j++; //filter out actions which don't have desired event type / unit flag
            //        } else
            //        {
            //            p.ProcessAuras(actions[i]);
            //        }
            //    }
                
            //}
            //Console.WriteLine("Press any key to exit.");
            //Console.ReadKey();
        }
    }

    class Player
    {
        List<Aura> Buffs;

        public Player()
        {
             Buffs = new List<Aura>();
        }

        public void ProcessAuras(LoggedEvent action)
        {
            if(action.TargetName != action.CasterName) return;
            if(action.SpellAuraType != "BUFF") return;
            if (action.EventType == "SPELL_AURA_APPLIED")
            {
                Buffs.Add(new Aura(action.SpellId, action.SpellName, 1));
            }
            else if (action.EventType == "SPELL_AURA_APPLIED_DOSE")
            {
                Buffs.Find(x => x.SpellName == action.SpellName).StackCount = action.SpellAuraCount; 
            }
            else if (action.EventType == "SPELL_AURA_REMOVED")
            {
                Buffs.Remove(Buffs.Find(x => x.SpellName == action.SpellName));
            }
        }

        public string GetPlayerAuras()
        {
            if (Buffs.Count == 0) return "";
            string buffs = "";
            for(int i = 0; i < Buffs.Count; i++)
            {
                buffs = Buffs[i] + ", " + buffs;
            }
            return buffs;
        }
    }

    class Aura
    {
        public int SpellId { get; }
        public string SpellName { get; }
        public int StackCount { get; set; }

        public Aura (int id, string name, int count)
        {
            SpellId = id;
            SpellName = name;
            StackCount = count;
        }

        public override string ToString()
        {
            return SpellName + (StackCount == 1 ? "" : "(" + StackCount + ")");
        }
    }
}
