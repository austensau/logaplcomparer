﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;


namespace LogAPLComparer
{
    public class WowAPICommunicator
    {
        private static readonly string clientID = "f6175f31a6b741988b1b18485184b077";
        private static readonly string clientSecret = "dew2b5sJoAdt4822atTOfqx9BnzSWVtz";
        private string AccessToken { get; set; }
        public DateTime TokenExpiration { get; private set; }

        private HttpClient Client { get; }
        private string JsonString { get; set; }

        static void Main(string[] args)
        {
            var communicator = new WowAPICommunicator();
            communicator.GetAccessTokenAsync().GetAwaiter().GetResult();
            Console.WriteLine(communicator.JsonString);
            Console.WriteLine(communicator.TokenExpiration);
            string mediaURL = communicator.GetSpellMediaAsync("133").GetAwaiter().GetResult();
            Console.WriteLine(mediaURL);
            communicator.DownloadIconAsync(mediaURL).GetAwaiter().GetResult();
        }

        public WowAPICommunicator()
        {
            Client = new HttpClient();
        }

        // Exchange client credentials for an access token.
        //  https://develop.battle.net/documentation/guides/using-oauth/client-credentials-flow
        //  Example curl req below -
        //  curl -u {client_id}:{client_secret} -d grant_type=client_credentials https://us.battle.net/oauth/token
        //  converted to c# using https://curl.olsh.me/

        /// <summary>
        /// Exchange client credentials for an access token which expires after ~24 hours.
        /// </summary>
        /// <returns></returns>
        public async Task GetAccessTokenAsync()
        {
            //Create an HTTP post request
            var request = new HttpRequestMessage(new HttpMethod("POST"), "https://us.battle.net/oauth/token");
            //Set the message string
            var base64authorization = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{clientID}:{clientSecret}"));
            //Set the headers
            request.Headers.TryAddWithoutValidation("Authorization", $"Basic {base64authorization}");
            request.Content = new StringContent("grant_type=client_credentials");
            request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/x-www-form-urlencoded");
            //Get the response
            var httpResponse = await Client.SendAsync(request);
            JsonString = await httpResponse.Content.ReadAsStringAsync();
            JObject jResults = JObject.Parse(JsonString);
            AccessToken = (string)jResults["access_token"];
            double minutesToExpiration = (double)jResults["expires_in"];
            TokenExpiration = DateTime.Now.AddSeconds(minutesToExpiration);
        }

        public async Task<string> GetSpellMediaAsync(string spellId)
        {
            //Refresh expired Access Token.
            if (DateTime.Compare(TokenExpiration, DateTime.Now) < 0)
            {
                await GetAccessTokenAsync();
            }
            string requestURL = "https://us.api.blizzard.com/data/wow/media/spell/" + spellId +
                "?namespace=static-us&locale=en_US&access_token=" + AccessToken;
            //Create an HTTP post request
            var request = new HttpRequestMessage(new HttpMethod("GET"), requestURL);
            var httpResponse = await Client.SendAsync(request);
            if(httpResponse.StatusCode == HttpStatusCode.OK)
            {
                JsonString = await httpResponse.Content.ReadAsStringAsync();
                JObject jResults = JObject.Parse(JsonString);
                return (string)jResults["assets"][0]["value"];
            }
            return null;
        }

        public async Task DownloadIconAsync(string requestURL)
        {
            var uri = new Uri(requestURL);
            var filename = Path.GetFileName(uri.LocalPath);
            if (filename is not null)
            {
                var path = @"C:\Users\Austen\source\repos\LogAPLComparer\Thumbnails\" + filename;
                if(!File.Exists(path))
                {
                    var httpResponse = await Client.SendAsync(new HttpRequestMessage(new HttpMethod("GET"), uri));
                    var stream = await httpResponse.Content.ReadAsStreamAsync();
                    using var fs = new FileStream(path, FileMode.CreateNew);
                    stream.CopyTo(fs);
                }
            }
        }
    }
}
