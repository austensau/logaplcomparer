﻿using System;
using System.Collections.Generic;

namespace LogAPLComparer
{
    class LoggedEvent
    {
        private static string[] prefixes = { "SWING", "RANGE", "SPELL", "SPELL_PERIODIC", "SPELL_BUILDING", "SPELL_ABSORBED", "ENVIRONMENTAL" };
        private static string[] suffixes = { "_DAMAGE", "_MISSED", "_HEAL", "_ENERGIZE", "_DRAIN", "_LEECH", "_INTERRUPT", "_DISPEL", "_DISPEL_FAILED", "_STOLEN",
        "_EXTRA_ATTACKS", "_AURA_APPLIED", "_AURA_REMOVED", "_AURA_APPLIED_DOSE", "_AURE_REMOVED_DOSE", "_AURA_REFRESH", "_AURA_BROKEN",
        "_AURA_BROKEN_SPELL", "_CAST_START", "_CAST_SUCCESS", "_CAST_FAILED", "_INSTAKILL", "_DURABILITY_DAMAGE", "_DURABILITY_DAMAGE_ALL", "_CREATE",
        "_SUMMON", "_RESURRECT"};

        public string RawAction { get; }
        public DateTime EventTime { get; }
        public string EventType { get; }      // 0
        public string CasterId { get; }       // 1
        public string CasterName { get; }     // 2
        public string CasterUnitFlag { get; } // 3
        public string CasterPowerType { get; }// 4
        public string TargetId { get; }       // 5
        public string TargetName { get; }     // 6
        public string TargetUnitFlag { get; } // 7
        public string TargetPowerType { get; }// 8
        public int SpellId { get; }        // 9
        public string SpellName { get; }      // 10
        public string SpellSchool { get; }    // 11
        public string SpellAuraType { get; }  // 12 - Aura Applied / Removed only
        public int SpellAuraCount { get; } //13 - Dose only

        public LoggedEvent(string action)
        {
            RawAction = action;
            string[] dateTimeSplit = action.Split(new char[] { ' ' }, 4);
            string[] eventSplit = dateTimeSplit[3].Split(',');
            EventTime = CreateDateTime(dateTimeSplit);
            EventType = eventSplit[0];
            if (EventType == "SPELL_CAST_SUCCESS" || EventType == "SPELL_AURA_APPLIED" || EventType == "SPELL_AURA_APPLIED_DOSE" || EventType == "SPELL_AURA_REMOVED")
            {
                CasterId = eventSplit[1];
                CasterName = eventSplit[2];
                CasterUnitFlag = eventSplit[3];
                CasterPowerType = eventSplit[4];
                TargetId = eventSplit[5];
                TargetName = eventSplit[6];
                SpellId = int.Parse(eventSplit[9]);
                SpellName = eventSplit[10];
                SpellSchool = eventSplit[11];
                if(EventType.Contains("AURA"))
                    SpellAuraType = eventSplit[12];
                if (EventType.Contains("DOSE"))
                    SpellAuraCount = int.Parse(eventSplit[13]);
            }
        }

        public static List<LoggedEvent> LoadEvents(string url)
        {
            string[] fileLines = System.IO.File.ReadAllLines(url);
            var events = new List<LoggedEvent>(fileLines.Length);
            foreach (string line in fileLines)
            {
                events.Add(new LoggedEvent(line));
            }
            return events;
        }

        public static DateTime CreateDateTime(string[] dateSplit)
        {
            int year = DateTime.Today.Year;
            int month = int.Parse(dateSplit[0].Split('/')[0]);
            int day = int.Parse(dateSplit[0].Split('/')[1]);
            int hour = int.Parse(dateSplit[1].Split(':')[0]);
            int minute = int.Parse(dateSplit[1].Split(':')[1]);
            int second = int.Parse(dateSplit[1].Split(':')[2].Split('.')[0]);
            int millisecond = int.Parse(dateSplit[1].Split(':')[2].Split('.')[1]);
            
            return new DateTime(year, month, day, hour, minute, second, millisecond);
        }

        public override string ToString()
        {
            return CasterName + " " + EventType + " " + SpellName + (TargetName == "nil" ? "" : "on " + TargetName);
        }

    }
}
